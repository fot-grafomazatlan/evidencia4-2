function llenarDatos(){
    var limite = document.getElementById('limite').value;
    var Listanumeros = document.getElementById('numeros');
    let par = document.getElementById('porPares');
    let imp = document.getElementById('porImpares');
    let simetria = document.getElementById('EsSimetrico');
    var arreglo = [];

    
    while(Listanumeros.options.length>0){
        Listanumeros.remove(0);
    }

    for(let con = 0; con < limite; con++) {
        let aleatorio = Math.floor(Math.random()*(50)+1); 
        Listanumeros.options[con] = new Option(aleatorio, 'valor:' + con);
        arreglo[con] = aleatorio;
    
    }
    
    let orden = ordenarValoresSelect(arreglo);

    for(let con = 0; con<limite; con++){
        Listanumeros.options[con] = new Option(orden[con]);
    }

    par.innerHTML = valorPares(arreglo).toFixed(2) + "%"; 
    imp.innerHTML = valorImpares(arreglo).toFixed(2) + "%"; 

    
    let pares = valorPares(arreglo);
    let impares = valorImpares(arreglo);

    if(pares - impares > 25 || impares - pares > 25){
        simetria.innerHTML = "No es Simetrico";
    }else {
        simetria.innerHTML = "Si es Simetrico";
    }



}




function ordenarValoresSelect(numeros){
    let arr = numeros, longitudOrdenMayor = numeros.length;
    let band = false;

    while(!band){
        band = true;
        for(let i=0; i<longitudOrdenMayor; i++){
                                             
            if(arr[i] > arr[i+1]){
                aux = arr[i+1];   
                arr [i+1] = arr [i]; 
                arr[i] = aux; 
                band = false;
            }
        }
    }
    return arr;
}

function alerta(){
    valor = document.getElementById('limite').value;
    errorValor = document.getElementById('errorLimite');
    porP = document.getElementById('porPares');
    porImp = document.getElementById('porImpares');
    sim = document.getElementById('EsSimetrico');

    valor== "" || valor== 0 ? errorValor.style.visibility = 'visible': errorValor.style.visibility = 'hidden' ;

    if(valor==0){

        alert("Faltan datos por capturar");
        porP.innerHTML = "";
        porImp.innerHTML = "";
        sim.innerHTML = "";
    }
}


function valorImpares(numeros){
    let contador = 0; 
    let arr = numeros;
    let numerosL = numeros.length;
    for(let i=0; i<numerosL; i++){
        if(arr[i]%2 != 0){ 

            contador++;
        }
    }
    contador = ((contador * 100) / numerosL);
    return contador;

}

function valorPares(numeros){
    let contador = 0; 
    let arr = numeros;
    let numerosL = numeros.length;
    for(let i=0; i<numerosL; i++){
        if(arr[i]%2 == 0){ 

            contador++;
        }
    }
    contador = ((contador * 100) / numerosL);
    return contador;

    

}


